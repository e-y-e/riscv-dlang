# RISC-V LLVM

## License

All patches are submitted under the University of Illinois/NCSA Open Source
License [as used by LLVM](http://llvm.org/docs/DeveloperPolicy.html#license).
