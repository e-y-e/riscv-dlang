# RISC-V LDC

## License

All patches are submitted under the BSD license [as used by LDC]
(https://github.com/ldc-developers/ldc/blob/master/LICENSE).
