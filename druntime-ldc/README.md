# RISC-V druntime (LDC)

## License

All patches are submitted under the Boost Software License [as used by druntime]
(https://github.com/ldc-developers/druntime/blob/master/LICENSE).
