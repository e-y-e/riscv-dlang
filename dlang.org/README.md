# RISC-V dlang.org

## License

All patches are submitted under the Boost Software License [as used by
dlang.org](https://github.com/dlang/dlang.org/blob/master/LICENSE.txt).
