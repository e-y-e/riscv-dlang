# RISC-V Dlang

## About

This repository hosts a number of patches that implement a RISC-V target for LDC
(the LLVM D Compiler) contained within the relevant directories relating to the
projects that they apply to. The purpose of this repository is to maintain a set
of clean patches that implement the RISC-V target in order to provide a
reference for when future modifications are made on the RISC-V target, whether
the purpose is to fix bugs or to modify the behaviour of the target.

## License

See each project directory for relevant licensing information for patches.
